package ie.gmit.sw.data;

import org.encog.ml.data.MLDataSet;
import org.encog.ml.data.basic.BasicMLDataSet;

import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Path;
import java.util.List;
import java.util.Objects;
import java.util.StringJoiner;
import java.util.logging.Logger;

public class DataBuilder {
    private static final Logger LOGGER = Logger.getLogger(DataBuilder.class.getName());

    private final List<NGramVectorBuilder> vectorBuilders;
    private final Path wiliFile;

    // hidden constructor
    private DataBuilder(List<NGramVectorBuilder> vectorBuilders, Path trainingFile){
        this.vectorBuilders = vectorBuilders;
        this.wiliFile = trainingFile;
    }

    /**
     * Factory method.
     * @param vectorBuilders input vector builders
     * @param file from which {@link MLDataSet} should be created
     * @return new instance
     */
    public static DataBuilder of(List<NGramVectorBuilder> vectorBuilders, Path file){
        return new DataBuilder(vectorBuilders, file);
    }

    /**
     * Process {@link DataBuilder#wiliFile} using {@link DataBuilder#vectorBuilders}
     * @return processed file as data set
     * @throws IOException as in {@link Files#lines(Path)}
     */
    public MLDataSet getTrainingDataSet() throws IOException {
        var languageEntryFactory = new LanguageEntryFactory(vectorBuilders);

        return Files.lines(wiliFile)
                .map(languageEntryFactory::entryOf)
                .filter(Objects::nonNull)
                .map(LanguageEntry::asDataPair)
                .collect(
                        BasicMLDataSet::new,
                        BasicMLDataSet::add,
                        (a, b) -> a.forEach(b::add)
                )
                ;
    }

    /**
     * Getter
     * @return vector builders
     */
    public List<NGramVectorBuilder> getVectorBuilders() {
        return vectorBuilders;
    }

    /**
     * Gets input vector information as the string.
     * @return input vector information
     */
    public String vectorInfo(){
        var sj = new StringJoiner(",\n\t" );
        vectorBuilders.forEach(it -> sj.add("size: "+ it.vectorSize() +", n-gram: "+ it.ngramFactory().getType()));
        return sj.toString();
    }

    @Override
    public String toString() {
        var clazz = this.getClass().getName();
        var sj = new StringJoiner(",\n\t", clazz +": {\n\t", "\n}");
        vectorBuilders.forEach(it -> sj.add(it.toString()));
        return sj.toString();
    }
}
