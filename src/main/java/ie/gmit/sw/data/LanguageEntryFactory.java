package ie.gmit.sw.data;

import java.util.List;
import java.util.concurrent.atomic.AtomicInteger;
import java.util.function.IntPredicate;
import java.util.logging.Logger;
import java.util.stream.DoubleStream;

/**
 * This class is a factory for the {@link LanguageEntry}ies.
 */
public class LanguageEntryFactory {
    private static final Logger LOGGER = Logger.getLogger(LanguageEntryFactory.class.getName());
    private final List<NGramVectorBuilder> vectorBuilders;

    /**
     * Constructor to build input vector.
     * @param vectorBuilders input vector settings.
     */
    public LanguageEntryFactory(List<NGramVectorBuilder> vectorBuilders){
        this.vectorBuilders = vectorBuilders;
    }

    /**
     * Create {@link LanguageEntry} from a given string.
     * @param entryText given data
     * @return created fom given string
     */
    public LanguageEntry entryOf(String entryText){
        var i = entryText.lastIndexOf('@');
        var languageText = entryText.substring(i + 1).trim();

        Language language = Language.NONE;
        try {
            language = Language.valueOf(languageText);
        } catch (IllegalArgumentException e){
            LOGGER.warning("Language not found. "+ e.getMessage());
        }
        var dataText = entryText.substring(0, i).toLowerCase();
        final var filtered = filterText(dataText);

        double [] data = vectorBuilders.stream()
                .map(vb -> vb.apply(filtered))
                .flatMapToDouble(DoubleStream::of)
                .toArray();
        return new LanguageEntry(language, data);
    }

    /**
     * Filters given text by removing unnesesary charavters
     * @param text unfiltered text
     * @return filtered text
     */
    public static String filterText(String text) {
        final var bracketCounter = new AtomicInteger();
        final var spacesCounter = new AtomicInteger();

        IntPredicate bracketSkipper = i -> {
            if (i == '(') {
                bracketCounter.incrementAndGet();
                return false;
            } else if (i == ')') {
                bracketCounter.decrementAndGet();
                return false;
            }

            return bracketCounter.get() < 1;
        };

        IntPredicate spaceFilter = i -> {
            if (Character.isSpaceChar(i)) {
                spacesCounter.incrementAndGet();
            } else {
                spacesCounter.set(0);
            }

            return spacesCounter.get() < 2;
        };

        IntPredicate noDigit = cp -> !Character.isDigit(cp);

        IntPredicate alphabetic = Character::isAlphabetic;

        IntPredicate punctuation = ch ->
                ch == ',' || ch == '\'' || ch == '.' || ch =='?'
                || ch == '!' || ch == '¿' || ch == '¡';

        IntPredicate allowedChars = alphabetic.or(Character::isSpaceChar).or(punctuation);

        return text.codePoints()
                // Remove text between the brackets. I noticed what
                // text between brackets are usually in English no
                // mater the language text is writen on.
                .filter(bracketSkipper)

                // Remove digits. Even dates add some information to
                // the language structure, I noticed given text was
                // containing many random digits without any meaning
                // to the actual language structure.
                .filter(noDigit)

                // Remove other characters. I noticed text was containing
                // some random characters which looked like not a part of
                // the actual language. At firs I was removing punctuation
                // but later I allowed it as it adds extra information to
                // the language structure.
                .filter(allowedChars)

                // Remove any repeating space. Due data preparation I noticed
                // there was continues spaces were appearing inside the text.
                .filter(spaceFilter)

                // Put all back to string.
                .collect(
                        StringBuilder::new,
                        StringBuilder::appendCodePoint,
                        StringBuilder::append)
                .toString()
                ;
    }
}
