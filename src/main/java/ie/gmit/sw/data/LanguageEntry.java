package ie.gmit.sw.data;

import org.encog.ml.data.MLDataPair;
import org.encog.ml.data.basic.BasicMLData;
import org.encog.ml.data.basic.BasicMLDataPair;

/**
 * This record is DTO for the training language information
 */
public record LanguageEntry(Language language, double[] data){
    public MLDataPair asDataPair() {
        var ideal = new BasicMLData(language.toVector());
        var inputData = new BasicMLData(data);

        return new BasicMLDataPair(inputData, ideal);
    }
}
