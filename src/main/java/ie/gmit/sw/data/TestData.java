package ie.gmit.sw.data;

import org.encog.ml.data.MLData;
import org.encog.ml.data.basic.BasicMLData;

import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Path;
import java.util.List;
import java.util.logging.Logger;
import java.util.stream.DoubleStream;
import java.util.stream.Stream;

/**
 * Used for test data creation.
 */
public class TestData {
    private static final Logger LOGGER = Logger.getLogger(TestData.class.getName());
    private final List<NGramVectorBuilder> vectorBuilders;
    private final Path file;

    private TestData(List<NGramVectorBuilder> vectorBuilders, Path file) {
        this.vectorBuilders = vectorBuilders;
        this.file = file;
    }

    /**
     * Factory method
     * @param vectorBuilders input vector structure
     * @param file from which we creating test data
     * @return new instance
     */
    public static TestData of(List<NGramVectorBuilder> vectorBuilders, Path file) {
        return new TestData(vectorBuilders, file);
    }

    /**
     * Builds test data as {@link MLData}
     * @return test data
     */
    public MLData build() {
        Stream<String> lineStream = null;
        try {
            lineStream = Files.lines(file);
        } catch (IOException e) {
            LOGGER.warning(e.getMessage());
            e.printStackTrace();
            return new BasicMLData(new double[0]);
        }

        var filtredText = lineStream
                .map(String::trim)
                .map(LanguageEntryFactory::filterText)
                .flatMapToInt(String::codePoints)
                .collect(
                        StringBuilder::new,
                        StringBuilder::appendCodePoint,
                        StringBuilder::append
                )
                .toString()
                ;

        double [] data = vectorBuilders.stream()
                .map(vb -> vb.apply(filtredText))
                .flatMapToDouble(DoubleStream::of)
                .toArray();

        return new BasicMLData(data);
    }
}
