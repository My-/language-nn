package ie.gmit.sw.data.ngram;

/**
 * {@link NGram} with {@code 1} characters in size.
 *
 */
public record OneGram(char ch) implements NGram {
    private final static NGramType ngramType = NGramType.ONEGRAM;

    /**
     * New instance from given data
     * @param data as text
     * @return new instance
     */
    public static OneGram from(CharSequence data){
        if(data.length() != ngramType.getSize()){
            var msg = String.format("Given value doesnt mach required size: %s, was: %s", ngramType.getSize(), data.length());
            throw new IllegalArgumentException(msg);
        }
        var ch = data.charAt(0);
        return new OneGram(ch);
    }

    @Override
    public int getValue() {
        return hashCode();
    }

    @Override
    public NGramType getType() {
        return ngramType;
    }

    @Override
    public boolean equals(Object obj) {
        if(obj instanceof OneGram oneGramg){
            return oneGramg.ch == ch;
        }
        return false;
    }

    @Override
    public int hashCode() {
        return ch;
    }

    @Override
    public String toString() {
        return """
                OneGram: {value: %s}"""
                .formatted(getValue());
    }
}
