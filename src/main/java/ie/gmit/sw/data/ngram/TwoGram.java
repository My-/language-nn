package ie.gmit.sw.data.ngram;

/**
 * {@link NGram} with {@code 2} characters in size.
 *
 */
public record TwoGram(int value) implements NGram{
    private final static NGramType ngramType = NGramType.TWOGRAM;

    @Override
    public int getValue() {
        return hashCode();
    }

    @Override
    public NGramType getType() {
        return ngramType;
    }

    @Override
    public boolean equals(Object obj) {
        if( obj instanceof TwoGram nGram){
            return nGram.value == value;
        }
        return false;
    }

    @Override
    public int hashCode() {
        return value;
    }

    @Override
    public String toString() {
        return "TwoGram{" +
                "value=" + value +
                '}';
    }

    /**
     * Creates new {@link TwoGram} from given data {@link CharSequence}
     * @param data as text
     * @return new instance
     */
    public static TwoGram from(CharSequence data){
        if(data.length() != ngramType.getSize()){
            var msg = String.format("Given value doesnt mach required size: %s, was: %s", ngramType.getSize(), data.length());
            throw new IllegalArgumentException(msg);
        }
        int i = data.charAt(0);
        i <<= 16;
        i |= data.charAt(1);
        return new TwoGram(i);
    }
}
