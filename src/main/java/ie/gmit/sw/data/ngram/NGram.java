package ie.gmit.sw.data.ngram;

/**
 * Contract for all n-grams.
 */
public interface NGram {
    /**
     * Gets {@link NGram} value as {@link int}
     * @return value
     */
    public int getValue();

    /**
     * Gets type as {@link NGramType}
     * @return type
     */
    public NGramType getType();
}


