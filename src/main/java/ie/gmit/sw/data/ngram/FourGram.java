package ie.gmit.sw.data.ngram;

/**
 * {@link NGram} with {@code 4} characters in size.
 *
 */
public record FourGram(long value) implements NGram{
    private final static NGramType ngramType = NGramType.FOURGRAM;

    @Override
    public int getValue() {
        return hashCode();
    }

    @Override
    public NGramType getType() {
        return ngramType;
    }

    @Override
    public boolean equals(Object obj) {
        if(obj instanceof FourGram nGram){
            return nGram.value == value;
        }
        return false;
    }

    @Override
    public int hashCode() {
        return Long.hashCode(value);
    }

    @Override
    public String toString() {
        return "FourGram{" +
                "str=" +ngramAsString(value) +
                ", value=" +value +
                '}';
    }

    /**
     * New instance from a given data
     * @param data as text
     * @return new instance
     */
    public static FourGram from(CharSequence data){
        if(data.length() != ngramType.getSize()){
            var msg = String.format("Given value doesnt mach required size: %s, was: %s", ngramType.getSize(), data.length());
            throw new IllegalArgumentException(msg);
        }
        var l = 0L;
        for(int i = 0; i < ngramType.getSize(); i += 1){
            var ch = data.charAt(i);
            l <<= 16;
            l |= ch;
        }
        return new FourGram(l);
    }

    public static String ngramAsString(long ngram){
        var arr = new char[4];
        var l = ngram;

        for(int i = 3; i >= 0; i -= 1){
            arr[i] = (char)l;
            l >>= 16;
        }

        return new String(arr);
    }
}
