package ie.gmit.sw.data.ngram;

/**
 * {@link NGram} with {@code 6} characters in size.
 *
 */
public record SixGram(int value) implements NGram {
    private final static NGramType ngramType = NGramType.SIXGRAM;

    @Override
    public int getValue() {
        return value;
    }

    @Override
    public NGramType getType() {
        return ngramType;
    }

    @Override
    public boolean equals(Object obj) {
        if(obj instanceof SixGram o){
            return o.value == value;
        }
        return false;
    }

    @Override
    public int hashCode() {
        return value;
    }

    @Override
    public String toString() {
        return """
               SixGram: { value: %s }
               """.formatted(value);
    }

    /**
     * Creates new instance
     * @param data as text
     * @return new instance
     */
    public static SixGram from(CharSequence data) {
        return new SixGram(data.toString().hashCode());
    }
}
