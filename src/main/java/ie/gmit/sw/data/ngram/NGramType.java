package ie.gmit.sw.data.ngram;

/**
 * Holds information about all {@link NGram}
 */
public enum NGramType {
    ONEGRAM(1),
    TWOGRAM(2),
    FOURGRAM(4),
    SIXGRAM(6),
    ;

    private final int size;

    NGramType(int size){
        this.size = size;
    }

    public int getSize(){
        return this.size;
    }
}
