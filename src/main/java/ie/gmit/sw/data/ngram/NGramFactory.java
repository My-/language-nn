package ie.gmit.sw.data.ngram;


import java.util.Objects;

/**
 * {@link NGram} factory.
 */
public class NGramFactory {
    private final NGramType type;

    // hidden constructor
    private NGramFactory(NGramType type){
        this.type = type;
    }

    /**
     * Factory method
     * @param type of {@link NGram} factory should make
     * @return new factory
     */
    public static NGramFactory of(NGramType type){
        return new NGramFactory(type);
    }

    /**
     * Gets what type {@link NGram}s does factory makes.
     * @return
     */
    public NGramType getType() {
        return type;
    }

    /**
     * Creates {@link NGram} from a given data.
     * @param data as text
     * @return new created ngram
     */
    public NGram createNGram(CharSequence data){
        return switch (type){
            case ONEGRAM -> OneGram.from(data);
            case TWOGRAM -> TwoGram.from(data);
            case FOURGRAM -> FourGram.from(data);
            case SIXGRAM -> SixGram.from(data);
        };
    }

    @Override
    public String toString() {
        return """
                %s: {ngramType: %S}"""
                .formatted(this.getClass().getName(), type);
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (!(o instanceof NGramFactory)) return false;
        NGramFactory that = (NGramFactory) o;
        return type == that.type;
    }

    @Override
    public int hashCode() {
        return Objects.hash(type);
    }
}
