package ie.gmit.sw.data;


import ie.gmit.sw.Utilities;
import ie.gmit.sw.data.ngram.NGram;
import ie.gmit.sw.data.ngram.NGramFactory;

import java.util.Objects;
import java.util.function.BiFunction;
import java.util.function.BinaryOperator;
import java.util.function.Function;
import java.util.logging.Logger;
import java.util.stream.IntStream;

/**
 * This class is responsible for input vector creation. This class represents single
 * part of the input vector. Because this class is implements {@link Function} interface
 * it can be used instead of {@link Function}.
 */
public record NGramVectorBuilder(int vectorSize, NGramFactory ngramFactory) implements Function<CharSequence, double[]>{
    private static final Logger LOGGER = Logger.getLogger(NGramVectorBuilder.class.getName());

    @Override
    public double[] apply(CharSequence text) {
        return getNGramData(text, ngramFactory, vectorSize);
    }



    @Override
    public boolean equals(Object obj) {
        if( obj instanceof NGramVectorBuilder b){
            return b.ngramFactory.equals(ngramFactory)
                    && b.vectorSize == vectorSize;
        }
        return false;
    }

    @Override
    public int hashCode() {
        return Objects.hash(vectorSize, ngramFactory);
    }

    @Override
    public String toString() {
        return """
                %s: { vectorSize: %d, ngramFactory: %s}"""
                .formatted(this.getClass().getName(), vectorSize, ngramFactory);
    }

    private static double[] getNGramData(CharSequence text, NGramFactory ngramFactory, int size){
        final var lastIndex = text.length() -ngramFactory.getType().getSize();
        final var inputVector = new double[size];
        BiFunction<double[], NGram, double[]> accumulator = (arr, ngram) -> {
            var i = ngram.getValue() % arr.length;
            if(i < 0){
                i *= -1;
            }
            arr[i]++;
            return arr;
        };
        BinaryOperator<double[]> combiner = (arr1, arr2) -> {
            if(arr1.length != arr2.length){
                LOGGER.warning("Arrays are not the same, arr1: "+  arr1.length +", arr2: "+ arr2.length);
                throw new IllegalArgumentException("Arrays length are not the same.");
            }
            for (int i = 0; i < arr1.length; i++) {
                arr1[i] += arr2[i];
            }
            return arr1;
        };

        var data = IntStream.range(0, lastIndex)
                .mapToObj(i -> ngramFactory.createNGram(text.subSequence(i, i +ngramFactory.getType().getSize())))
                .reduce(inputVector, accumulator, combiner)
                ;

        var norm = Utilities.normalize(data, 0, 1);

        return norm;
    }
}
