package ie.gmit.sw.app;

import ie.gmit.sw.Utilities;
import ie.gmit.sw.data.TestData;
import org.encog.persist.PersistError;

import java.io.File;
import java.io.IOException;
import java.nio.file.InvalidPathException;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.util.Date;
import java.util.Scanner;

/**
 * UI for the app. This is a {@code V2} of UI. Firs
 * version was abandoned due lack of time, but if
 * completed would be more feature proof. Firs version
 * can be found in {@link ie.gmit.sw.legacy.ui.UI}
 */
public class UI {
    private final App app;

    private UI(App app){
        this.app = app;
    }

    public static UI of(App app) {
        return new UI(app);
    }

    private static final Scanner scanner = new Scanner(System.in);


    public void homePage() {
        char choice = '!';
        do {
            System.out.println("""
                    
                    
                    ---------------------------
                       Neural network
                    --------------------------- 
                        [1] - Input Vector info.  
                        [2] - Train network.
                        [3] - Load pretrained network.
                        [4] - Predict.
                       
                        [x] - Exit                
                    ---------------------------
                    """
            );
            choice = takeChar();

            switch (choice) {
                case '1' -> inputVectorInfo();
                case '2' -> trainNN();
                case '3' -> loadNetwork();
                case '4' -> testNN();
            }
        } while (choice != 'x');
    }

    private void loadNetwork() {
        System.out.println("""
                ----------------------------------------------------
                    Enter path to the pretrained network file
                ----------------------------------------------------
                
                """
        );

        var filePath = scanner.next();

        File file = null;
        try {
            var path = Paths.get(filePath);
            file = new File(path.toUri());
        } catch (InvalidPathException e){
            System.err.println("Error: "+ e.getMessage());
        }

        try {
            app.getNetwork().load(file);
        }catch (
            PersistError e){
            System.err.println("Error: "+ e.getMessage());
            return;
        }

        System.out.println("Network loaded from: "+ file);

    }

    private void inputVectorInfo() {
        char choice = '!';
        do {
            System.out.println("""
                       
                    
                    ---------------------------
                       Input Vector Info
                    ---------------------------   
                       %s
                    ---------------------------
                       [x] - Back.               
                    --------------------------- 
                    """.formatted(app.getDataBuilder().vectorInfo())
            );
            choice = takeChar();

        } while (choice != 'x');
    }

    private void trainNN() {
        char choice = '!';
        do {
            System.out.println("""
                    
                    
                    ---------------------------
                       Train - Neural network
                    ---------------------------   
                    %s
                    ---------------------------
                       [1] - Go.
                       
                       [x] - Back.               
                    --------------------------- 
                    """.formatted(app.getNetwork().toString())
            );
            choice = takeChar();

            switch (choice) {
                case '1' -> startTraining();
                case '2' -> testNN();
            }
        } while (choice != 'x');
    }

    private void startTraining(){
        try {
            app.startTraining();
        } catch (IOException e) {
            e.printStackTrace();
        }

        Utilities.saveNeuralNetwork(
                app.getNetwork().getNetwork(), new Date().toString().replaceAll(" ", "_")
        );


        System.out.println("-------------------");
        // ToDo:
        System.out.println(app.test());
    }

    private void testNN() {
        char choice = '!';
        do {
            System.out.println("""
                       
                    ---------------------------
                       Test - Neural network
                    ---------------------------   
                       %s
                    ---------------------------
                       [1] - Choose file.
                       
                       [x] - Back.               
                    ---------------------------
                    """.formatted(app.getNetwork().toString())
            );
            choice = takeChar();

            switch (choice) {
                case '1' -> chooseFile();
                case '2' -> testNN();
            }
        } while (choice != 'x');
    }

    private void chooseFile() {
//        scanner.next();
        System.out.println("""
                ----------------------------------------
                    Enter path for language text file
                ----------------------------------------
                
                """
        );

        var file = scanner.next();

        Path filePath = null;
        try {
            filePath = Paths.get(file);

        } catch (InvalidPathException e){
            System.err.println("Error: "+ e.getMessage());
        }

        System.out.println("Preparing data...");
        var vectorsSettings = app.getDataBuilder().getVectorBuilders();
        var data = TestData.of(vectorsSettings, filePath).build();
        System.out.println("Data is prepared.");

        System.out.println("Testing NN ...");
        var guess = app.getNetwork().test(data);

        System.out.println("""
                -------------------------------
                    NN predicts language is: %s
                -------------------------------
                """.formatted(guess)
        );

    }

    ///////////////// helpers ///////////////

    private static char takeChar() {
        System.out.println("Waiting for input... ");
        return scanner.next().trim().toLowerCase().charAt(0);
    }



    private boolean wrongChoice(char choice, char...options) {
        for (int i = 0; i < options.length; i++) {
            if( choice == options[i]) {
                return false;
            }
        }
        return true;
    }

}
