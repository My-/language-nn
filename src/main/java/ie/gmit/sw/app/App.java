package ie.gmit.sw.app;

import ie.gmit.sw.data.NGramVectorBuilder;
import ie.gmit.sw.nn.NeuralNetwork;
import ie.gmit.sw.nn.stats.StatsDisplayer;
import ie.gmit.sw.nn.stats.TrainingStats;
import ie.gmit.sw.data.DataBuilder;
import org.encog.Encog;
import org.encog.ml.data.MLDataSet;
import org.encog.neural.networks.layers.BasicLayer;

import java.io.IOException;
import java.nio.file.Path;
import java.util.List;
import java.util.Objects;
import java.util.concurrent.SubmissionPublisher;
import java.util.logging.Logger;

/**
 * Main application class. This class is used in {@link UI} class
 * to gain control over whole application.
 */
public class App  {
    private static final Logger LOGGER = Logger.getLogger(App.class.getName());


    private final NeuralNetwork neuralNetwork;
    private final DataBuilder dataBuilder;
    private final int trainingTime;
    private final double dropout;

    private  MLDataSet trainingSet;

    private App(DataBuilder dataBuilder, NeuralNetwork neuralNetwork, int trainingTime, double dropout) {
        this.dataBuilder = dataBuilder;
        this.neuralNetwork = neuralNetwork;
        this.trainingTime = trainingTime;
        this.dropout = dropout;
    }

    public static Builder getBuilder() {
        return new Builder();
    }

    public DataBuilder getDataBuilder() {
        return dataBuilder;
    }


    public NeuralNetwork getNetwork() {
        return neuralNetwork;
    }


    public void startTraining() throws IOException {
        var publisher = new SubmissionPublisher<TrainingStats>();
        neuralNetwork.setStatsPublisher(publisher);

        var subscriber = new StatsDisplayer();
        publisher.subscribe(subscriber);

        this.trainingSet = dataBuilder.getTrainingDataSet();
        neuralNetwork.train(trainingSet, dropout, trainingTime);

    }

    public String test() {
        if(Objects.isNull(trainingSet)){
            return "you need firs to train network";
        }
        var stats = neuralNetwork.testAll(trainingSet);

        return stats.toString();
    }


    public void stopTraining() {
        throw new UnsupportedOperationException("Unimplemented");
    }




    public void exit() {
        Encog.getInstance().shutdown();
        System.exit(0);
    }



    public static class Builder {
        private Path trainingFile;
        private DataBuilder dataBuilder;
        private NeuralNetwork neuralNetwork;
        private int trainingTime;
        private double dropout;

        Builder (){
        }

        public Builder setTrainingFile(Path trainingFile) {
            this.trainingFile = trainingFile;
            return this;
        }

        public Builder setVectorBuilders(List<NGramVectorBuilder> vectorBuilders) {
            if(Objects.isNull(trainingFile)){
                throw new UnsupportedOperationException("Training file is not set.");
            }
            LOGGER.info("Building vector...");
            this.dataBuilder = DataBuilder.of(vectorBuilders, trainingFile);
            LOGGER.info("... done.");
            return this;
        }

        public Builder setNeuralNetworkLayers(List<BasicLayer> layerSettings) {
            this.neuralNetwork = NeuralNetwork.of(layerSettings).build();
            return this;
        }

        public Builder setTrainingTime(int trainingTime) {
            this.trainingTime = trainingTime;
            return this;
        }

        public Builder setDropout(double dropout) {
            this.dropout = dropout;
            return this;
        }

        public App build() {
            return new App(dataBuilder, neuralNetwork, trainingTime, dropout);
        }


    }
}
