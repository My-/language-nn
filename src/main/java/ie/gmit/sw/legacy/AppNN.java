package ie.gmit.sw.legacy;

import ie.gmit.sw.nn.NeuralNetwork;
import ie.gmit.sw.nn.stats.TestStats;
import ie.gmit.sw.nn.stats.TrainingStats;
import org.encog.neural.networks.layers.BasicLayer;

import java.io.File;
import java.nio.file.Path;
import java.util.List;
import java.util.concurrent.Flow;

public interface AppNN {
    public void createNetwork(List<BasicLayer> layerSettings);

    public void loadNetwork(File file);

    public void saveNetwork(File file);

    public NeuralNetwork getNetwork();

    public Flow.Publisher<TrainingStats> startTraining(int seconds);

    public void stopTraining();

    public Flow.Publisher<TestStats> test();

    public Flow.Publisher<TestStats> test(Path file);

    public Flow.Publisher<TestStats> test(String text);

    public void exit();
}
