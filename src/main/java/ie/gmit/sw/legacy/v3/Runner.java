package ie.gmit.sw.legacy.v3;

import ie.gmit.sw.Utilities;
import ie.gmit.sw.data.DataBuilder;
import ie.gmit.sw.data.NGramVectorBuilder;
import ie.gmit.sw.nn.NeuralNetwork;
import ie.gmit.sw.data.Language;
import ie.gmit.sw.data.ngram.NGramFactory;
import ie.gmit.sw.data.ngram.NGramType;
import ie.gmit.sw.nn.stats.StatsDisplayer;
import ie.gmit.sw.nn.stats.TrainingStats;
import org.encog.Encog;
import org.encog.engine.network.activation.ActivationReLU;
import org.encog.engine.network.activation.ActivationSigmoid;
import org.encog.engine.network.activation.ActivationSoftMax;
import org.encog.neural.networks.layers.BasicLayer;

import java.io.IOException;
import java.nio.file.Path;
import java.util.Date;
import java.util.List;
import java.util.concurrent.SubmissionPublisher;
import java.util.logging.Logger;

public class Runner {
    public static final int TRAIN_SEC = 160;

    private static final Logger LOGGER = Logger.getLogger(Runner.class.getName());

    public static void main(String[] args) throws IOException {

//        var app = App.getInstance();
//        var ui = app.getUi();
//        ui.start();


        List<NGramVectorBuilder> vectorBuilders = List.of(
//                new NGramVectorBuilder(128, NGramFactory.of(NGramType.ONEGRAM)),
                new NGramVectorBuilder(380, NGramFactory.of(NGramType.TWOGRAM))     // 380
//                new NGramVectorBuilder(1500, NGramFactory.of(NGramType.FOURGRAM))
        );


        var inputs = vectorBuilders.stream()
                .mapToInt(NGramVectorBuilder::vectorSize)
                .sum()
                ;
        var outputs = Language.values().length;

        List<BasicLayer> layerSettings = List.of(
                new BasicLayer(new ActivationSigmoid(), true, inputs),
                new BasicLayer(new ActivationReLU(), true, 250),// Two grams 250
//                new BasicLayer(new ActivationReLU(), true, 500),
//                new BasicLayer(new ActivationReLU(), true, 300),
                new BasicLayer(new ActivationSoftMax(), true, outputs)
        );

        var dataBuilder = DataBuilder.of(vectorBuilders, Path.of(ie.gmit.sw.Runner.WILI_FILE));
        var dataSet = dataBuilder.getTrainingDataSet();
        System.out.println("DataSet is build.");

        var nn = NeuralNetwork.of(layerSettings);
        nn.build();

        var publisher = new SubmissionPublisher<TrainingStats>();
        nn.setStatsPublisher(publisher);

        var subscriber = new StatsDisplayer();
        publisher.subscribe(subscriber);

        System.out.println(dataBuilder);
        System.out.println(nn);

        var dropout = 0.87; // 0.82
        var trainSeconds = 160;
        nn.train(dataSet, dropout, trainSeconds);

        System.out.println("done training");

        var stats = nn.testAll(dataSet);
        System.out.println(stats);

        Utilities.saveNeuralNetwork(
                nn.getNetwork(), new Date().toString()
                        +String.format(" (%.1f%s)", stats.ratio() *100, "%")
        );

        Encog.getInstance().shutdown();

    }




}



