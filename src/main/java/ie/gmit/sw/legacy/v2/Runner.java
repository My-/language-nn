package ie.gmit.sw.legacy.v2;

import ie.gmit.sw.Utilities;
import ie.gmit.sw.data.Language;
import ie.gmit.sw.legacy.language.LanguageData;
import ie.gmit.sw.data.ngram.NGram;
import ie.gmit.sw.data.ngram.NGramFactory;
import ie.gmit.sw.data.ngram.NGramType;
import org.encog.ml.data.MLData;
import org.encog.ml.data.MLDataSet;
import org.encog.ml.data.basic.BasicMLData;
import org.encog.ml.data.basic.BasicMLDataPair;
import org.encog.ml.data.basic.BasicMLDataSet;

import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Paths;
import java.util.Objects;
import java.util.concurrent.atomic.AtomicInteger;
import java.util.function.BiFunction;
import java.util.function.BinaryOperator;
import java.util.function.IntPredicate;
import java.util.logging.Logger;
import java.util.stream.IntStream;


public class Runner {

    private static final Logger LOGGER = Logger.getLogger(Runner.class.getName());

    private static NGramFactory ngramFactory = NGramFactory.of(NGramType.FOURGRAM);

    public static void main(String[] args) throws IOException {

//        var nn = new NeuralNetwork();



    }

    public static MLDataSet getDataSet() throws IOException {
        var wiliFile = Paths.get(ie.gmit.sw.Runner.WILI_FILE);

        return Files.lines(wiliFile)
//                .parallel()
                .map(line -> {
                    var i = line.lastIndexOf('@');
                    var language = Language.valueOf(line.substring(i + 1).trim());
                    var text = filterText(line.substring(0, i)).toLowerCase();
                    if(text.length() < 30){
                        System.out.println("Short text in language: "+ language +", text: "+ text);
                        return null;
                    }
                    MLData ideal = new BasicMLData(language.toVector());
//                    System.out.println(ideal); // LOG
                    var dataVector = getNGramData(text);
                    var data = new BasicMLData(Utilities.normalize(dataVector, 0, 1));
//                    System.out.println(data.toString());
                    return new BasicMLDataPair(data, ideal);
                })
//                .peek(it -> System.out.println(it))
                .filter(Objects::nonNull)
//                .peek(it -> {
//                    System.out.println(it.toString());
//                })
                .collect(
                        BasicMLDataSet::new,
                        BasicMLDataSet::add,
                        (a, b) -> a.forEach(b::add)
                )
                ;
    }

    private static String filterText(String text) {
        final var bracketCounter = new AtomicInteger();
        final var spacesCounter = new AtomicInteger();

        IntPredicate bracketSkipper = i -> {
            if (i == '(') {
                bracketCounter.incrementAndGet();
                return false;
            } else if (i == ')') {
                bracketCounter.decrementAndGet();
                return false;
            }

            return bracketCounter.get() < 1;
        };

        IntPredicate spaceFilter = i -> {
            if (Character.isSpaceChar(i)) {
                spacesCounter.incrementAndGet();
            } else {
                spacesCounter.set(0);
            }

            return spacesCounter.get() < 2;
        };

        return text.codePoints()
                .filter(bracketSkipper)
                .filter(cp -> !Character.isDigit(cp))
                .filter(cp -> Character.isAlphabetic(cp) || Character.isSpaceChar(cp))
                .filter(spaceFilter)
//                .peek(System.out::println)
                .collect(StringBuilder::new,
                        StringBuilder::appendCodePoint,
                        StringBuilder::append)
                .toString()
                ;
    }

    private static double[] getNGramData(String text){
        var lastIndex = text.length() -ngramFactory.getType().getSize();
        final var inputVector = new double[LanguageData.DATA_SIZE];
        BiFunction<double[], NGram, double[]> accumulator = (arr, ngram) -> {
            var i = ngram.getValue() % arr.length;
            if(i < 0){
//                LOGGER.warning("Negative i: {}, ngram: {}", i, ngram);
                i *= -1;
            }
            arr[i]++;
            return arr;
        };
        BinaryOperator<double[]> combiner = (arr1, arr2) -> {
            if(arr1.length != arr2.length){
                LOGGER.warning("Arrays are not the same, arr1: "+  arr1.length +", arr2: "+ arr2.length);
                throw new IllegalArgumentException("Arrays length are not the same.");
            }
            for (int i = 0; i < arr1.length; i++) {
                arr1[i] += arr2[i];
            }
            return arr1;
        };

        return IntStream.range(0, lastIndex)
                .mapToObj(i -> ngramFactory.createNGram(text.substring(i, i +ngramFactory.getType().getSize())))
                .reduce(inputVector, accumulator, combiner)
                ;
    }
}
