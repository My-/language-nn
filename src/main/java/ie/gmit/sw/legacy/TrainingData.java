package ie.gmit.sw.legacy;

import org.encog.ml.data.MLData;
import org.encog.ml.data.MLDataPair;
import org.encog.ml.data.MLDataSet;

import java.util.Iterator;
import java.util.Spliterator;
import java.util.concurrent.Flow;
import java.util.concurrent.locks.Lock;
import java.util.concurrent.locks.ReentrantLock;
import java.util.function.Consumer;
import java.util.logging.Logger;

/**
 *
 */
public class TrainingData implements MLDataSet, Flow.Subscriber<MLDataPair> {
    private static final Logger LOGGER = Logger.getLogger(TrainingData.class.getName());

    private final MLDataSet storage;
    private final Lock lock;

    private Flow.Subscription subscription;
    private volatile boolean isCompleated;


    public TrainingData(MLDataSet storage) {
        this.storage = storage;
        lock = new ReentrantLock(true);
    }

    private MLDataSet getStorage() {
        // lock

        return storage;
    }

    /* Subscriber */

    @Override
    public void onSubscribe(Flow.Subscription subscription) {
        this.subscription = subscription;
        subscription.request(1);
    }

    @Override
    public void onNext(MLDataPair item) {
        storage.add(item);
        subscription.request(1);
    }

    @Override
    public void onError(Throwable throwable) {
        System.err.println("Error: "+ throwable.getMessage());
        throwable.printStackTrace();
    }

    @Override
    public void onComplete() {
        LOGGER.info("""
                Done. All data received (%d) by %s."""
                .formatted(storage.size(), this.getClass().getName())
        );
        isCompleated = true;
    }

    /* MLDataSet */

    @Override
    public int getIdealSize() {
        return getStorage().getIdealSize();
    }

    @Override
    public int getInputSize() {
        return getStorage().getInputSize();
    }

    @Override
    public boolean isSupervised() {
        return getStorage().isSupervised();
    }

    @Override
    public long getRecordCount() {
        return getStorage().getRecordCount();
    }

    @Override
    public void getRecord(long index, MLDataPair pair) {
        getStorage().getRecord(index, pair);
    }

    @Override
    public MLDataSet openAdditional() {
        return getStorage().openAdditional();
    }

    @Override
    public void add(MLData data1) {
        getStorage().add(data1);
    }

    @Override
    public void add(MLData inputData, MLData idealData) {
        getStorage().add(inputData, idealData);
    }

    @Override
    public void add(MLDataPair inputData) {
        getStorage().add(inputData);
    }

    @Override
    public void close() {
        getStorage().close();
    }

    @Override
    public int size() {
        return getStorage().size();
    }

    @Override
    public MLDataPair get(int index) {
        return getStorage().get(index);
    }

    @Override
    public Iterator<MLDataPair> iterator() {
        return getStorage().iterator();
    }

    @Override
    public void forEach(Consumer<? super MLDataPair> action) {
        getStorage().forEach(action);
    }

    @Override
    public Spliterator<MLDataPair> spliterator() {
        return getStorage().spliterator();
    }



}
