package ie.gmit.sw.legacy.ui;

import ie.gmit.sw.legacy.AppNN;
import ie.gmit.sw.data.ngram.NGramFactory;
import ie.gmit.sw.data.ngram.NGramType;
import ie.gmit.sw.data.NGramVectorBuilder;

import java.nio.file.Path;
import java.util.*;
import java.util.function.Function;
import java.util.function.Supplier;

enum MenuPages {ABOUT, EXIT, NN, NN_CREATE, NN_TRAIN, NN_TEST, NN_CREATE_NEW, NN_CREATE_LOAD, NN_TEST_TEXT, BUILD_VECTOR, HOME}

public class UI {
    static final String SEPARATOR = "---------------------------------";
    private static final Supplier NULL = () -> null;

    private final Path trainingFile;
    private AppNN app;

    private UI(Path traininngFile) {
        this.trainingFile = traininngFile;
    }

    public static UI of(Path traininngFile) {
        return new UI(traininngFile);
    }

    private final Map<MenuPages, Menu> menus = Map.of(
            // Home page
            MenuPages.HOME,
            new Menu(
                    new MenuHeader("Language detection"),
                    Map.of(
                            '1', new Action("About.", goToPage(MenuPages.ABOUT)),
                            '2', new Action("Build vector.", goToPage(MenuPages.BUILD_VECTOR)),
                            '3', new Action("Neural Network.", goToPage(MenuPages.NN)),
                            'x', new Action("Exit.", goToPage(MenuPages.EXIT))
                    ),
                    new MenuFooter(
                            """
                            Select one option by using keyboard
                            No touch screen support available"""
                    )
            ),
            // About page
            MenuPages.ABOUT,
            new Menu(
                    new MenuHeader("About project"),
                    Map.of(
                            'x', new Action("Go Back.", NULL)
                    ),
                    new MenuFooter(
                            """
                            Author: Mindaugas
                            Date: May, 2020"""
                    )
            ),
            // About page
            MenuPages.BUILD_VECTOR,
            new Menu(
                    new MenuHeader("Build Vector"),
                    Map.of(
                            '1', new Action("Vector builder", this::vectorBuilder),
                            'x', new Action("Go Back.", NULL)
                    ),
                    new MenuFooter(
                            """
                            Author: Mindaugas
                            Date: May, 2020"""
                    )
            ),
//            // Exit page
//            MenuPages.EXIT,
//            new Menu(
//                    new MenuHeader("Exit - are you sure?"),
//                    Map.of(
//                            'y', new Action("Yes", () -> {
//                                app.exit(); return null;
//                            }),
//                            'n', new Action("No", goToPage(MenuPages.HOME))
//                    ),
//                    new MenuFooter("")
//            ),
            // NN page
            MenuPages.NN,
            new Menu(
                    new MenuHeader("Neural Network"),
                    Map.of(
                            '1', new Action("Create", goToPage(MenuPages.NN_CREATE)),
                            '2', new Action("Train", goToPage(MenuPages.NN_TRAIN)),
                            '3', new Action("Test", goToPage(MenuPages.NN_TEST)),
                            'x', new Action("Go Back.", NULL)
                    ),
                    new MenuFooter("")
            ),
            // NN create page
            MenuPages.NN_CREATE,
            new Menu(
                    new MenuHeader("Neural Network - Create"),
                    Map.of(
                            '1', new Action("New", goToPage(MenuPages.NN_CREATE_NEW)),
                            '2', new Action("Load existing", goToPage(MenuPages.NN_CREATE_LOAD)),
                            'x', new Action("Go Back.", NULL)
                    ),
                    new MenuFooter("")
            ),
            // NN create page
            MenuPages.NN_CREATE_NEW,
            new Menu(
                    new MenuHeader("Neural Network - Create - New"),
                    Map.of(
//                            new MenuAction('1', "New", goToPage(MenuPages.NN_CREATE)),
//                            new MenuAction('2', "Load", goToPage(MenuPages.NN_TRAIN)),
                            'x', new Action("Go Back.", NULL)
                    ),
                    new MenuFooter("Current neural network"+app.getNetwork().toString())
            ),
            // NN create page
            MenuPages.NN_CREATE_LOAD,
            new Menu(
                    new MenuHeader("Neural Network - Create - Load Existing"),
                    Map.of(
//                            new MenuAction('1', "New", goToPage(MenuPages.NN_CREATE)),
//                            new MenuAction('2', "Load", goToPage(MenuPages.NN_TRAIN)),
                            'x', new Action("Go Back.", NULL)
                    ),
                    new MenuFooter("Under construction....")
            ),
            // NN train page
            MenuPages.NN_TRAIN,
            new Menu(
                    new MenuHeader("Neural Network - Training"),
                    Map.of(
//                            new MenuAction('1', "New", goToPage(MenuPages.NN_CREATE)),
//                            new MenuAction('2', "Load", goToPage(MenuPages.NN_TRAIN)),
                            'x', new Action("Stop & Go Back.", NULL)
                    ),
                    new MenuFooter("Under construction....")
            ),
            // NN test page
            MenuPages.NN_TEST,
            new Menu(
                    new MenuHeader("Neural Network - Testing"),
                    Map.of(
                            '1', new Action("Test using training data set", NULL),
                            '2', new Action("Test using file", NULL),
                            '3', new Action("Test by entering text", goToPage(MenuPages.NN_TEST_TEXT)),
                            'x', new Action("Go Back.", NULL)
                    ),
                    new MenuFooter("Under construction....")
            ),
            // NN test page
            MenuPages.NN_TEST_TEXT,
            new Menu(
                    new MenuHeader("Neural Network - Testing - Text"),
                    Map.of(
//                            new MenuAction('1', "New", goToPage(MenuPages.NN_CREATE)),
//                            new MenuAction('2', "Load", goToPage(MenuPages.NN_TRAIN)),
                            'x', new Action("Go Back.", NULL)
                    ),
                    new MenuFooter("Under construction....")
            )
    );





    //********************** UI's **********************//
    private boolean confirm(String mesage) {
        var header = new MenuHeader(mesage);
        System.out.println("""
                %s
                    [y] - YES
                    [n] - NO
                """.formatted(header)
        );

        return 'y' == Menu.getInputChar();
    }

    private Object vectorBuilder() {
        var list = new ArrayList<NGramVectorBuilder>(5);

        do {
            list.add(getVectorBuilder());
        } while( confirm("Add another n-gram?"));

//        app = App.getBuilder()
//                .setTrainingFile(trainingFile)
//                .setVectorBuilders(list)
//                .build();

        return null;
    }

    private NGramVectorBuilder getVectorBuilder() {
        var header = new MenuHeader("Vector builder");
        var choice = '!';

        do {
            System.out.println("""
                %s   
                Choose n-gram:             
                    [1] - One-gram
                    [2] - Two-gram
                    [4] - Four-gram
                    [6] - Six-gram
                """.formatted(header)
            );

            choice = Menu.getInputChar();
        } while(wrongChoice(choice, '1', '2', '4', '6'));

        var ngram = switch (choice) {
            case '1' -> NGramType.ONEGRAM;
            case '2' -> NGramType.TWOGRAM;
            case '4' -> NGramType.FOURGRAM;
            case '6' -> NGramType.SIXGRAM;
            default -> throw new IllegalStateException("Unexpected value: " + choice);
        };

        var size = 100;
        var wrong = true;

        do {
            System.out.println("""
                %s   
                Choose enter size:
                """.formatted(header)
            );

            try {
                size = Menu.scanner.nextInt();
                if(size < 10){
                    throw new InputMismatchException("Value is to small");
                } else if ( size > 10_000){
                    throw new InputMismatchException("Value is to large");
                } else {
                    wrong = false;
                }

            } catch (InputMismatchException e) {
                System.out.println(e.getMessage());
            }
        } while (wrong);

        return new NGramVectorBuilder(size, NGramFactory.of(ngram));
    }





    //********************** Helpers **********************//

    public void start(){
        menus.get(MenuPages.HOME).run();
    }


    private Supplier goToPage(MenuPages page) {
        return () -> menus.get(page).run();
    }

    private static Function<MenuInputs, MenuOutputs> exitApp(){
        System.out.println("Bye");
        System.exit(0);
        return null;
    }


    private static boolean wrongChoice(char ch, char...other) {
        for (int i = 0; i < other.length; i++) {
            if(ch == other[i]){
                return false;
            }
        }
        return true;
    }

}


