package ie.gmit.sw.legacy.ui;

import java.util.*;
import java.util.function.Consumer;
import java.util.function.Supplier;
import java.util.stream.Collectors;

public record Menu(MenuHeader header, Map<Character, Action>actions, MenuFooter footer){
    public static final Scanner scanner = new Scanner(System.in);
    private static final Consumer<Map.Entry<Character, Action>> display = entry ->
            System.out.println(String.format("    [%c] - %s", entry.getKey(), entry.getValue()));

    public Object run() {
        char choise = '_';
        Object res = null;

        do {
            // show header
            System.out.println(header);
            // show actions
            actions.entrySet().forEach(display);
            // show footer
            System.out.println(footer);
            // get user input
            final var option = getInputChar();
            // find `option` action
            res = actions.get(option).get();
            choise = option;
        } while(choise != 'x');

        return res;
    }

    public static char getInputChar() {
        System.out.println("Waiting for input... ");
        return scanner.next().trim().toLowerCase().charAt(0);
    }

}

record Action(String description, Supplier action) implements Supplier {

    @Override
    public boolean equals(Object obj) {
        if ( obj instanceof Action ma){
            return ma.action == action();
        }
        return false;
    }

    @Override
    public int hashCode() {
        return Objects.hash(description, action);
    }

    @Override
    public String toString() {
        return description;
    }


    @Override
    public Object get() {
        return action.get();
    }
}



record MenuHeader(String label){
    @Override
    public String toString() {
        return """
                %s    
                    %s
                %1$s
                """.formatted(UI.SEPARATOR, label);
    }
}

record MenuFooter(String notes){
    @Override
    public String toString() {
        return """
                
                %s
                %s
                """.formatted(UI.SEPARATOR, notes());
    }

    public String notes() {
        if(notes.isBlank()){
            return "";
        }

        return Arrays.stream(notes.split("\\r?\\n"))
                .map(String::trim)
                .collect(Collectors.joining("\n\t- ", "Notes:\n\t- ", ""));
    }
}

record MenuInputs(Map<String, Object>inputs){
}

record MenuOutputs(Map<String, Object> outputs){
}
