package ie.gmit.sw.legacy.language;


import ie.gmit.sw.data.Language;

import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Path;
import java.util.concurrent.Flow;
import java.util.concurrent.SubmissionPublisher;
import java.util.concurrent.atomic.AtomicInteger;
import java.util.function.IntPredicate;
import java.util.logging.Logger;

/**
 * ToDo:
 *      - remove short text lines
 *      -
 * Ref:
 * - https://community.oracle.com/docs/DOC-1006738
 */
public class LanguageEntryPublisher implements Flow.Publisher<LanguageEntry> {
    private static final Logger LOGGER = Logger.getLogger(LanguageEntryPublisher.class.getName());

    private final Path wiliFile;
    // ToDO: add field for function which wil transfer (filler) text.
    private final SubmissionPublisher<LanguageEntry> publisher = new SubmissionPublisher<>();

    private LanguageEntryPublisher(Path wiliFile) {
        this.wiliFile = wiliFile;
    }

    public static LanguageEntryPublisher of(Path wiliFilePath) {
        return new LanguageEntryPublisher(wiliFilePath);
    }

    @Override
    public void subscribe(Flow.Subscriber<? super LanguageEntry> subscriber) {
        publisher.subscribe(subscriber);
        LOGGER.info("Subscribed by: " + subscriber);
        try {
            readFile();
        } catch (IOException e) {
            subscriber.onError(e);
        }
    }

    private void readFile() throws IOException {
        System.out.println("reading file");
        Files.lines(wiliFile)
//                .parallel()
                .map(line -> {
                    var i = line.lastIndexOf('@');
                    var language = Language.valueOf(line.substring(i + 1).trim());
                    var text = filterText(line.substring(0, i));
                    return new LanguageEntry(language, text);
                })
//                .peek(it -> System.out.println(it))
                .forEach(publisher::submit);
    }

    private static String filterText(String text) {
        final var bracketCounter = new AtomicInteger();
        final var spacesCounter = new AtomicInteger();

        IntPredicate bracketSkipper = i -> {
            if (i == '(') {
                bracketCounter.incrementAndGet();
                return false;
            } else if (i == ')') {
                bracketCounter.decrementAndGet();
                return false;
            }

            return bracketCounter.get() < 1;
        };

        IntPredicate spaceFilter = i -> {
            if (Character.isSpaceChar(i)) {
                spacesCounter.incrementAndGet();
            } else {
                spacesCounter.set(0);
            }

            return spacesCounter.get() < 2;
        };

        return text.codePoints()
                .filter(bracketSkipper)
                .filter(cp -> !Character.isDigit(cp))
                .filter(cp -> Character.isAlphabetic(cp) || Character.isSpaceChar(cp))
                .filter(spaceFilter)
//                .peek(System.out::println)
                .collect(StringBuilder::new,
                        StringBuilder::appendCodePoint,
                        StringBuilder::append)
                .toString()
                ;
    }
}

record LanguageEntry(Language language, String text) {
}
