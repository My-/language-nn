package ie.gmit.sw.legacy.language;

import ie.gmit.sw.data.Language;
import org.encog.ml.data.MLDataPair;
import org.encog.ml.data.basic.BasicMLData;
import org.encog.ml.data.basic.BasicMLDataPair;

import java.util.Arrays;

public record LanguageData(Language language, double[] data) {
    public final static int DATA_SIZE = 10000;

    public MLDataPair toDataPair(){
        var mlData = new BasicMLData(data);
        var ideal = new BasicMLData(language.toVector());

        return new BasicMLDataPair(mlData, ideal);
    }

    @Override
    public String toString() {
        return """
            LanguageData: {
                language: %s, data: %s
            }
            """.formatted(language, Arrays.toString(data));
    }
}
