package ie.gmit.sw.legacy.language;

import ie.gmit.sw.Utilities;
import ie.gmit.sw.data.ngram.NGram;
import ie.gmit.sw.data.ngram.NGramFactory;
import ie.gmit.sw.data.ngram.NGramType;

import java.util.concurrent.Flow;
import java.util.concurrent.SubmissionPublisher;
import java.util.function.BiFunction;
import java.util.function.BinaryOperator;
import java.util.logging.Logger;
import java.util.stream.IntStream;

public class LanguageDataProcessor
        extends SubmissionPublisher<LanguageData>
        implements Flow.Processor<LanguageEntry, LanguageData>
{
    public static final int INITIUAL_REQUEST_AMOUNT = 10;
    private static final Logger LOGGER = Logger.getLogger(LanguageDataProcessor.class.getName());

    private final NGramType ngramType;
    private final NGramFactory ngramFactory;

//    private Flow.Subscriber<? super LanguageData> subscriber;
    private Flow.Subscription subscription;

    private LanguageDataProcessor(NGramType nrramType){
        this.ngramType = nrramType;
        this.ngramFactory = NGramFactory.of(nrramType);
    }

    public static LanguageDataProcessor of(NGramType ngramType){
        return new LanguageDataProcessor(ngramType);
    }

//    @Override
//    public void subscribe(Flow.Subscriber<? super LanguageData> subscriber) {
//        this.subscriber = subscriber;
//        LOGGER.info("Subscribed by: "+ subscriber);
//    }

    @Override
    public void onSubscribe(Flow.Subscription subscription) {
        this.subscription = subscription;
        LOGGER.info("Subscribed on: "+ subscription);
        this.subscription.request(1);
    }

    @Override
    public void onNext(LanguageEntry item) {
        LOGGER.fine("got: "+ item);
//        System.out.println(item);

        var ngramData = getNGramData(item.text());
        var normalizedData = Utilities.normalize(ngramData, 0, 1);
        var data = new LanguageData(item.language(), normalizedData);
        LOGGER.fine("sending: "+ data);
//        System.out.println(data);
        submit(data);
        subscription.request(1);
    }

    @Override
    public void onError(Throwable throwable) {
        LOGGER.warning("Error: "+ throwable.getMessage());
        onError(throwable);
//        subscriber.onError(throwable);
    }

    @Override
    public void onComplete() {
        close();
    }

    private double[] getNGramData(String text){
        var lastIndex = text.length() -ngramType.getSize();
        final var inputVector = new double[LanguageData.DATA_SIZE];
        BiFunction<double[], NGram, double[]> accumulator = (arr, ngram) -> {
            var i = ngram.getValue() % arr.length;
            if(i < 0){
//                LOGGER.warning("Negative i: {}, ngram: {}", i, ngram);
                i *= -1;
            }
            arr[i]++;
            return arr;
        };
        BinaryOperator<double[]> combiner = (arr1, arr2) -> {
            if(arr1.length != arr2.length){
                LOGGER.warning("Arrays are not the same, arr1: "+  arr1.length +", arr2: "+ arr2.length);
                throw new IllegalArgumentException("Arrays length are not the same.");
            }
            for (int i = 0; i < arr1.length; i++) {
                arr1[i] += arr2[i];
            }
            return arr1;
        };

        return IntStream.range(0, lastIndex)
                .mapToObj(i -> ngramFactory.createNGram(text.substring(i, i +ngramType.getSize())))
                .reduce(inputVector, accumulator, combiner)
                ;
    }
}


