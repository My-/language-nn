package ie.gmit.sw.nn.stats;

/**
 * Holds information about testing stats.
 */
public record TestStats(int total, int ok){
    public double ratio(){
        return 1.0 * ok / total;
    }

    /**
     * Calculates sensitivity
     * @return sensitivity value
     */
	public double sensitivity(){
        return total* 1.0 / (total + total - ok);
    }


    @Override
    public String toString() {
        return """
				TestStats: {
					total: %d,
					right: %d,
					ratio: %.1f%s
					sensitivity: %.2f
				}"""
                .formatted(total, ok, (ratio()*100), "%", sensitivity());
    }
}
