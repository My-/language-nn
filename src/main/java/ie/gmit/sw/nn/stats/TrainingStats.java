package ie.gmit.sw.nn.stats;

import java.util.Objects;
import java.util.function.DoubleFunction;

/**
 * Holds information about training network information.
 */
public record TrainingStats(int epoch, double epochTime, double trainError, double estimateTimeLeft) {
    private static final DoubleFunction<String> addSign = d -> d < 0 ? "" : "+";

    /**
     * Print stats.
     * @param prev previous stats
     * @return stats formatted
     */
    public String statsWithChange(TrainingStats prev) {
        if (Objects.isNull(prev)) {
            return stats();
        }
        var timeChange = epochTime - prev.epochTime;
        var errorChange = trainError - prev.trainError;

        return """
                Training stats:
                    Epoch: %d,
                    Epoch time: %.2f sec (%s%.2f),
                    Train error: %.4f%s (%s%.2f),
                    Estimated time left: %.0f sec
                """
                .formatted(
                        epoch,
                        epochTime, addSign.apply(timeChange), timeChange,
                        (trainError * 100), "%", addSign.apply(errorChange), (errorChange * 100),
                        estimateTimeLeft
                );
    }

    private String stats() {
        return """
                Training stats:
                    Epoch: %d,
                    Epoch time: %.2f sec,
                    Train error: %.4f%s,
                    Estimated time left: %.0f sec
                """
                .formatted(
                        epoch,
                        epochTime,
                        trainError * 100, "%",
                        estimateTimeLeft
                );
    }


    @Override
    public String toString() {
        return """
                TrainingStats: {
                    epoch: %d,
                    epochTime: %.2f,
                    error: %f
                }"""
                .formatted(epoch, epochTime, trainError);
    }


}
