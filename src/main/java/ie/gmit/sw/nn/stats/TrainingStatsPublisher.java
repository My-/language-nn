package ie.gmit.sw.nn.stats;


import java.util.concurrent.Flow;
import java.util.concurrent.SubmissionPublisher;
import java.util.logging.Logger;

public class TrainingStatsPublisher implements Flow.Publisher<TrainingStats> {
    private static final Logger LOGGER = Logger.getLogger(TrainingStatsPublisher.class.getName());
    private final SubmissionPublisher<TrainingStats> publisher = new SubmissionPublisher<>();

    @Override
    public void subscribe(Flow.Subscriber<? super TrainingStats> subscriber) {
        publisher.subscribe(subscriber);
        LOGGER.info("Subscribed by: " + subscriber);
    }

    public void submit(TrainingStats stats) {
        publisher.submit(stats);
    }


}
