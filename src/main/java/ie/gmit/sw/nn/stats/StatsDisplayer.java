package ie.gmit.sw.nn.stats;

import java.util.concurrent.Flow;

/**
 * Subscriber for the {@link TrainingStats} publisher.
 * This class prints items to the console.
 */
public class StatsDisplayer implements Flow.Subscriber<TrainingStats> {
    private Flow.Subscription subscription;
    private TrainingStats prev;

    @Override
    public void onSubscribe(Flow.Subscription subscription) {
        this.subscription = subscription;
        subscription.request(1);
    }

    @Override
    public void onNext(TrainingStats item) {
        System.out.println(item.statsWithChange(prev));
        prev = item;
        subscription.request(1);
    }

    @Override
    public void onError(Throwable throwable) {
        System.err.println("Error: "+throwable.getMessage());
        throwable.printStackTrace();
    }

    @Override
    public void onComplete() {
        System.out.println("Finnish training.");
    }
}
