package ie.gmit.sw.nn;

import java.io.File;
import java.util.List;
import java.util.Objects;
import java.util.StringJoiner;
import java.util.concurrent.SubmissionPublisher;
import java.util.function.BiFunction;
import java.util.function.Function;
import java.util.function.Predicate;
import java.util.logging.Logger;
import java.util.stream.StreamSupport;

import ie.gmit.sw.data.Language;
import ie.gmit.sw.nn.stats.TestStats;
import ie.gmit.sw.nn.stats.TrainingStats;
import org.encog.ml.data.MLData;
import org.encog.ml.data.MLDataPair;
import org.encog.ml.data.MLDataSet;
import org.encog.ml.data.folded.FoldedDataSet;
import org.encog.neural.networks.BasicNetwork;
import org.encog.neural.networks.layers.BasicLayer;
import org.encog.neural.networks.training.cross.CrossValidationKFold;
import org.encog.neural.networks.training.propagation.resilient.ResilientPropagation;
import org.encog.persist.EncogDirectoryPersistence;

/**
 * This class responsible for creation, training and validation of neural networks.
 */
public class NeuralNetwork {
    private static final Logger LOGGER = Logger.getLogger(NeuralNetwork.class.getName());

    private final List<BasicLayer> layersSettings;

    private BasicNetwork network;
    private SubmissionPublisher<TrainingStats> statsPublisher;

    // private constructor
    private NeuralNetwork(List<BasicLayer> layersSettings) {
        this.layersSettings = layersSettings;
    }

    /**
     * Factory method.
     * @param layersSettings the way nn should be created
     * @return new created instance
     */
    public static NeuralNetwork of(List<BasicLayer> layersSettings) {
        return new NeuralNetwork(layersSettings);
    }

    /**
     * Gets stats publisher which can subscribe to {@link TrainingStats} subscriber.
     * @return publisher
     */
    public SubmissionPublisher<TrainingStats> getStatsPublisher() {
        if (Objects.isNull(statsPublisher)) {
            LOGGER.warning("Returning null! Stats publisher is not set.");
        }
        return statsPublisher;
    }

    /**
     * Sets {@link TrainingStats} publisher.
     * @param statsPublisher we want to use.
     * @return this object.
     */
    public NeuralNetwork setStatsPublisher(SubmissionPublisher<TrainingStats> statsPublisher) {
        this.statsPublisher = statsPublisher;

        return this;
    }

    /**
     * Build Neural network
     *
     * @return this object
     */
    public NeuralNetwork build() {
        network = new BasicNetwork();

        layersSettings.forEach(network::addLayer);

        network.getStructure().finalizeStructure();
        network.reset();

        LOGGER.info("NN build done.");
        return this;
    }

    /**
     * Loads nn from given file.
     * @param file nn file
     * @return this object.
     */
    public NeuralNetwork load(File file) {
        network = (BasicNetwork) EncogDirectoryPersistence.loadObject(file);
        return this;
    }

    /**
     * Saves nn to file
     * @param file were to save
     * @return this object
     */
    public NeuralNetwork save(File file) {
        EncogDirectoryPersistence.saveObject(file, network);
        return this;
    }


    /**
     * Trains network.
     * @param trainingSet training data set
     * @param dropout dropout rate
     * @param trainSeconds training time in seconds
     */
    public void train(MLDataSet trainingSet, double dropout, int trainSeconds) {
        FoldedDataSet folded = new FoldedDataSet(trainingSet);
        ResilientPropagation train = new ResilientPropagation(network, folded);
        train.setDroupoutRate(dropout);
        System.out.println("dropout: " + train.getDropoutRate());

        CrossValidationKFold crossValidation = new CrossValidationKFold(train, 5);

        System.out.println("starting training...");
        BiFunction<Long, Long, Double> timeSpend = (start, end) -> (end - start) / Math.pow(10, 9);
        //Train the neural network
        int epoch = 0; //Use this to track the number of epochs

        var startTime = System.nanoTime();
        var epochStartTime = System.nanoTime();
        var endTime = 0L;

        // TRAINING...
        do {
            crossValidation.iteration();
            endTime = System.nanoTime();
            epoch++;
            var epochTime = timeSpend.apply(epochStartTime, endTime);
//			LOGGER.info("err: "+ crossValidation.getError()
//					+", epoch time: "+ epochTime);

            if (Objects.nonNull(statsPublisher)) {
                var timeLeft = trainSeconds - timeSpend.apply(startTime, endTime);
                var estimateTimeLeft = timeLeft + epochTime;
                var stats = new TrainingStats(epoch, timeSpend.apply(epochStartTime, endTime), crossValidation.getError(), estimateTimeLeft);
                epochStartTime = endTime;
                statsPublisher.submit(stats);
            }

        } while (timeSpend.apply(startTime, endTime) < trainSeconds);

        crossValidation.finishTraining();
        LOGGER.info("Training complete: error: " + train.getError()
                + ", epochs: " + epoch
                + ", total time: " + timeSpend.apply(startTime, endTime));

        if (Objects.nonNull(statsPublisher)) {
            statsPublisher.close();
        }
    }

    /**
     * Test all given data set
     * @param dataset data set to test
     * @return tes stats
     */
    public TestStats testAll(MLDataSet dataset) {
        Predicate<MLDataPair> test = pair -> {
            var ideal = pair.getIdeal().getData();
            var idealLanguage = Language.ofMax(ideal);

            var guessLanguage = test(pair.getInput());

            return idealLanguage == guessLanguage;
        };

        var ok = StreamSupport.stream(dataset.spliterator(), false)
                .filter(test)
                .count();

        return new TestStats(dataset.size(), (int) ok);
    }

    /**
     * Tries to predict given data
     * @param data given data
     * @return language it predicts
     */
    public Language test(MLData data) {
        MLData output = network.compute(data);
        double[] guess = output.getData();
        return Language.ofMax(guess);
    }

    /**
     * Getter
     * @return
     */
    public BasicNetwork getNetwork() {
        return network;
    }

    @Override
    public String toString() {
        Function<BasicLayer, CharSequence> toString = bl -> """
                BasicLayer: {activation: %s, neurons: %d}"""
                .formatted(
                        bl.getActivationFunction().getLabel(), bl.getNeuronCount()
                );

        var sj = new StringJoiner(",\n\t", "NeuralNetwork: {\n\t", "\n}");
        layersSettings.stream()
                .map(toString)
                .forEach(sj::add);

        return sj.toString() + "\n";
    }
}

