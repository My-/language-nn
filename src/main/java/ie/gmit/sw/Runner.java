package ie.gmit.sw;

import ie.gmit.sw.app.App;
import ie.gmit.sw.app.UI;
import ie.gmit.sw.data.Language;
import ie.gmit.sw.data.ngram.NGramFactory;
import ie.gmit.sw.data.ngram.NGramType;
import ie.gmit.sw.data.NGramVectorBuilder;
import org.encog.engine.network.activation.ActivationReLU;
import org.encog.engine.network.activation.ActivationSigmoid;
import org.encog.engine.network.activation.ActivationSoftMax;
import org.encog.neural.networks.layers.BasicLayer;

import java.nio.file.Paths;
import java.util.List;
import java.util.logging.Logger;

/**
 * Application starting point. In this class settings can be changed for the various
 * application behaviours.
 */
public class Runner {
	private static final Logger LOGGER = Logger.getLogger(Runner.class.getName());
	public static final String WILI_FILE = "data/wili-2018-Small-11750-Edited.txt";

	public static void main(String ...args){
		// check args
		if(args.length < 1 ){
			LOGGER.warning("No wili file provided.");
			return;
		}
		// wili file
		var wiliFile = Paths.get(args[0]);
		LOGGER.info("Using training file: "+ wiliFile);

		// data structure
		List<NGramVectorBuilder> vectorBuilders = List.of(
				new NGramVectorBuilder(380, NGramFactory.of(NGramType.TWOGRAM))     // 380
		);

		var inputs = vectorBuilders.stream()
				.mapToInt(NGramVectorBuilder::vectorSize)
				.sum()
				;
		var outputs = Language.values().length;

		// NN structure
		List<BasicLayer> layerSettings = List.of(
				new BasicLayer(new ActivationSigmoid(), true, inputs),
				new BasicLayer(new ActivationReLU(), true, 250),// Two grams 250
				new BasicLayer(new ActivationSoftMax(), true, outputs)
		);

		// Create App
		App app = App.getBuilder()
				.setTrainingFile(wiliFile)
				.setVectorBuilders(vectorBuilders)
				.setNeuralNetworkLayers(layerSettings)
				.setTrainingTime(160)
				.setDropout(0.83)
				.build();

		// start UI
		UI.of(app).homePage();
	}
}