# Language detection using Neural Network
**About:** This is an AI module supplementary project to replace exams due to COVID-19 lockdown. Because of it, we had about one week to work on it.  
For this project, we needed to create and train neural network so it could recognize the text for multiple languages. Data preparation was too part of this project.

**Notes:** This project is being written using Java-14 (preview) features and uses `encog` as a dependency. You need to pass `--enable-preview` parameter to `java` in order to make it work.

**Goals:** Project has a couple of goals:
- Using the given `wili` file prepare data for the neural network training.
- Create, train and test neural network with that data.
- Crate basic UI in terminal to demonstrate functionality.


### Data
For the data, we were given a `wili` file which is Wikipedia's web-site scraped text for the multiple languages. We needed to convert files content into multiple n-grams and feed it to Neural Network (NN) for training.

#### Text preparation
Before converting file text to the n-grams first text needs to be prepared. I knew how important clean data is for the NN so I decided to clean it (filter) on a couple of criteria. As Java Streams make core very readable I will use heavily commented actual code snipped to list them.
```java
private static String filterText(String text) {
   // ... lambdas here

   return text.codePoints()
            // Remove text between the brackets. I noticed what
            // text between brackets are usually in English no
            // mater the language text is writen on.
            .filter(bracketSkipper)

            // Remove digits. Even so dates add some information to
            // the language structure, I noticed given text was
            // containing many random digits without any meaning
            // to the actual language structure.
            .filter(noDigit)

            // Remove other characters. I noticed text was containing
            // some random characters which looked like not a part of
            // the actual language. At first, I was removing punctuation
            // too, but later I allowed it as it adds extra information to
            // the language structure.
            .filter(allowedChars)

            // Remove any repeating space. Due to data preparation, I noticed
            // there was continues spaces were appearing inside the text.
            .filter(spaceFilter)

            // Put all back to string.
            .collect(
                    StringBuilder::new,
                    StringBuilder::appendCodePoint,
                    StringBuilder::append)
            .toString()
            ;
}
```
After all preparation was done I did prune any text which was short (< 30 character long) as I believe short text would mess up NN training.

#### N-grams
When data was prepared, n-gram creation time comes. As at that time I didn't know what n-grams would work best I decided to add support for a couple of them and allow adding any other with ease. I achieved this by combining OOP and FP paradigms. To help better understand my design I created a dependency diagram.

![Data diagram](pictures/data-preparation-diagram.png)

One of parameters `DataBuilder` takes is `List` of `NGramVectorBuilder`'s. That list is used to build input vector **dynamically**. For example:
```java
List<NGramVectorBuilder> vectorBuilders = List.of(
        new NGramVectorBuilder(128, NGramFactory.of(NGramType.ONEGRAM)),
        new NGramVectorBuilder(380, NGramFactory.of(NGramType.TWOGRAM))
);

var dataBuilder = DataBuilder.of(vectorBuilders);
```
It would make input vector of total length `508`, *one-grams* would take first `128` positions followed by `380` *two-grams*. Adding more elements or changing existing ones would change the input vector accordingly. I choose to do this way as I wanted to change the input vector's data as easy as possible.  

`NGramVectorBuilder` implements `Function<CharSequence, double[]>` interface which allows it to be used as a function (lambda). List of `NGramVectorBuilder` is internally used by `LanguageEntryFactory` which is hidden (black diamond) so it can be replaced to any other if the need occurs without braking of `DataBuilder` user code. `LanguageEntryFactory` combines (concatenates) all different n-gram vectors into the one and creates `LanguageEntry` record (Java-14 feature) out of it. `LanguageEntry` record has a method which allows it to be converted to the `MLDataPair` where it can be collected into `MLDataSet` which is used to train NN. All this allowed mew to skip *csv* file step completely.  

As Java's `int` is `32 bit` integer and could fit up to 4 `char`'s inside I created custom hashing function for the one-grams, two-grams and four-grams. Implementation is using a bit-shifting technique we were shown in an another project. N-grams were implemented using Java-14 (preview) feature - `record`. I choose to use `record`s as they should reduce resources needed for the DTO (Data Transfer Object) plus syntax is nicer. N-grams diagram below shows their implementation. Note I didn't follow strict UML rules than creating these diagrams.

![N-Gram Diagram](pictures/ngram-diagram.png)


### Neural Network
 Neural Network (NN) was the most time consuming and least productive part of this project. Let me explain why. The requirement was to train NN to the `98%` precision and for the top marks it should be done under 3 min and over 10 min would be zero marks for this assignment part. So time was the main constrain is why I choose a training time spend as training exit condition. After countless runs with various configurations, the max accuracy I could achieve in under 3 min was a `73%` on a lucky day.

#### Code
 Before covering topologies I tried I want to cover a bit the way I was creating them in my code.  
 To allow change NN topologies with ease I decided to do same as I did with input vector creation - to pass `List` of `BasicLayer`s to the `NeuralNetwork` class. That way I could tweak all settings from one place - `Runner`. I need to note `UI` was build after NN was trained. Bellow is actual code example.
 ```java
 List<BasicLayer> layerSettings = List.of(
         new BasicLayer(new ActivationSigmoid(), true, inputs),
         new BasicLayer(new ActivationReLU(), true, 250),
         new BasicLayer(new ActivationSoftMax(), true, outputs)
 );

 var nn = NeuralNetwork.of(layerSettings);
 ```

#### Topologies
To me, it looks like to design NN is pure magic and try and error is the only way to do it. It is a bit funny as is exactly how NN works. NN takes random values (weights) and checks if the prediction is what the user wants (output layer). If the prediction is not good enough it adjusts values a bit (gradient descend) and checks again for the results. NN repeats this step until prediction is good enough. That was exactly what I was doing while trying to find the best NN configuration. If there was more time for the project, and I had way more computational power to hand I would create NN to find the best configuration for the language detection NN.

I had two main settings I could adjust for the NN I was working on:
- **input vector** - as mentioned earlier I could choose any number of n-grams or any combination of them at any length I wanted.
- **NN layers** - Similar story to input vectors, any number of hidden layers with any number of neurons to each and with handful activation functions I could choose for each layer.

###### Journey to the local minimum
I'll cover this section by covering the steps I take, and my reasoning while I made decisions. I don't know if it is an appropriate way to write, but it helps me to write easier.

Here was quite a lot of knobs could be turned. So, as training time was the main constrain and because more neurons means more computation first I tried to use small values for the NN and to use only four-grams:

```java
// input vector builder
List<NGramVectorBuilder> vectorBuilders = List.of(
        new NGramVectorBuilder(250, NGramFactory.of(NGramType.FOURGRAM))
);
// NN settings
List<BasicLayer> layerSettings = List.of(
      new BasicLayer(new ActivationSigmoid(), true, inputs),
      new BasicLayer(new ActivationSigmoid(), true, outputs)
);
```

Well, it was quick, but nothing more. *Ok, so if it is quick I can add more neurons and hopefully get better results.* I decided to add a single hidden layer too. For the hidden layer, I choose Relu activation as I used it in the previous project, *mnist* data set, and it worked very well.

```java
// input vector builder
List<NGramVectorBuilder> vectorBuilders = List.of(
        new NGramVectorBuilder(250, NGramFactory.of(NGramType.FOURGRAM))
);
// NN settings
List<BasicLayer> layerSettings = List.of(
      new BasicLayer(new ActivationSigmoid(), true, inputs),    
      new BasicLayer(new ActivationReLU(), true, 150),
      new BasicLayer(new ActivationSigmoid(), true, outputs)
);
```

Fast forward, many try later, the only thing what was changing is epoch running time... Firs I thought my data was prepared wrong. After many debugging hours I couldn't see anything suspicious caching my eye apart from the fact what output layer didn't sum to the 1 (100%) all the time. Switching output layers activation function to Softmax fixed the problem. Then I thought, maybe my laptop not fast enough... Maybe I need to use way bigger values and ignore running time for now:

```java
// input vector builder
List<NGramVectorBuilder> vectorBuilders = List.of(
        new NGramVectorBuilder(10_000, NGramFactory.of(NGramType.FOURGRAM))
);
// NN settings
List<BasicLayer> layerSettings = List.of(
      new BasicLayer(new ActivationSigmoid(), true, inputs),    
      new BasicLayer(new ActivationReLU(), true, 150),
      new BasicLayer(new ActivationSigmoid(), true, outputs)
);
```
Success! `60%`! well it was very slow but still first time I got something more then `10%`. Lets do even more:

```java
// input vector builder
List<NGramVectorBuilder> vectorBuilders = List.of(
        new NGramVectorBuilder(10_000, NGramFactory.of(NGramType.FOURGRAM))
);
// NN settings
List<BasicLayer> layerSettings = List.of(
      new BasicLayer(new ActivationSigmoid(), true, inputs),    
      new BasicLayer(new ActivationReLU(), true, 5_000),
      new BasicLayer(new ActivationReLU(), true, 1_000),
      new BasicLayer(new ActivationSigmoid(), true, outputs)
);
```
Boom, Java out of memory exception... Maybe a bit to dramatic adjustment I made. Well At least I know limits now. As here was no further improvement I could make I decided to ask in module forum about tips.

While I was waiting for a replay I decided to refactor code a bit and think about why 10k worked? Can I make it work with smaller input?

I am not sure if I think right but I guess is due to the fact what possible 4-grams for 26 letter alphabet (eg. English) is `26^4 = 456976`. When we fit them into 10k array we have way fewer collisions compare to the 100 sizes one and if we add other languages, which we do, especially those which doesn't use Latin letters we get way more collisions. All this makes training data noisier and harder to predict. So if I'm right to reduce input size I need to:
- increase hidden layer neurons or/and,
- decrease n-gram size.

At this point, I got replay at the forum plus I read the other student's forum questions and got a few more tips. First one is to use combined n-grams. As I was refactoring code at that time I decided to add that, multi n-gram functionality. Note: even I used `List` of `NGramVectorBuilders` in the example before only at this point I added that functionality.
```java
// input vector builder
List<NGramVectorBuilder> vectorBuilders = List.of(
        new NGramVectorBuilder(300, NGramFactory.of(NGramType.TWOGRAM)),
        new NGramVectorBuilder(300, NGramFactory.of(NGramType.FOURGRAM))
);
// NN settings
List<BasicLayer> layerSettings = List.of(
      new BasicLayer(new ActivationSigmoid(), true, inputs),    
      new BasicLayer(new ActivationReLU(), true, 300),
      new BasicLayer(new ActivationSigmoid(), true, outputs)
);
```
Sadly no luck... Ok, just to be sure let us run that crazy "10k hack" setup I had successes before I did refactor code, just to be sure. Oh, no... it is a debugging time, as it doesn't work anymore. Later, many debugging hours later, still no luck, and I didn't notice any suspicious parts.

At this time I got an idea to allow punctuation characters, I did was removing them before. Surprise, accuracy suddenly jumped: I was getting 10 times more correct results, which was `1107`, yes that is `9%` I didn't saw for a long time. So I decided to ignore my "10k hack" for now, and instead focus on trying other tips I got while browsing forum and internet searching foe ideas.

One of the tips I got was to use **dropout**, credit goes to the Ronan for pointing it out. After some head-scratching and googling I find out what it is the last parameter in `BasicLayer`s overloaded constructor... no comment, please, it was late, and I was very tired.  
After I applied dropout, I noticed my accuracy dropped a bit. It was weird and strange, so I asked Ronan about it. He didn't know whats happening but he pointed out what here is another way to setup dropout by:
```java
ResilientPropagation train = new ResilientPropagation(network, folded );
train.setDroupoutRate(0.5);
```
This time I noticed some improvement, thank you Ronan again. Some more adjustments an I got my best setup achieving `70%` under 3 min post of time:
```java
// input vector builder
List<NGramVectorBuilder> vectorBuilders = List.of(
        new NGramVectorBuilder(380, NGramFactory.of(NGramType.TWOGRAM))
);
// NN settings
List<BasicLayer> layerSettings = List.of(
      new BasicLayer(new ActivationSigmoid(), true, inputs),    
      new BasicLayer(new ActivationReLU(), true, 250),
      new BasicLayer(new ActivationSigmoid(), true, outputs)
);
// Dropout
var dropoout = 0.82;
```
I believe this my setup is just a local minimum and better one might be found, but how hard I tried I didn't manage to achieve that.

###### Other setup
Another setup which was promising is:
```java
List<NGramVectorBuilder> vectorBuilders = List.of(
        new NGramVectorBuilder(128, NGramFactory.of(NGramType.ONEGRAM)),
        new NGramVectorBuilder(380, NGramFactory.of(NGramType.TWOGRAM))
);

List<BasicLayer> layerSettings = List.of(
        new BasicLayer(new ActivationSigmoid(), true, inputs),
        new BasicLayer(new ActivationReLU(), true, 280),
        new BasicLayer(new ActivationSoftMax(), true, outputs)
);
// Dropout
var dropoout = 0.82;
```
Credit goes to Aaron and Ronan for ponting one-grams in forum. This setup has a better precission if runs longer.


###### NN Conclusion
Creating NN is dark voodoo magic, here are no hard rules what should work and what shouldn't. All it makes hard to create one. Is why I cover NN part the way I did, as my journey to the local minimum. 

After I got some progress I can make some conclusions about why it worked. Still, I have no explanation why my "10k hack" stopped working, why in encog library setting dropout one way works and does not work if set another way, I did dig into source code a bit to find out but with no luck.

I might be wrong, but to get a better pecision I would need to have more neurons and more hidden layers to come with collisions. More data would help too. CNN might help, but I couldnt figureout how to build one in encog. I had an idea to build NN out of few smaler ones in the way I could pass different input vector to each separatly and later thouse NN would merdge. Say one-gram to one and two-gram to another one or two-grams to each but with different hash finctions.

I had many ideas but very little time.

### UI
As I build all application flexible, with ability to tweek most of its parts, I wanted to build UI to take advantage ot of that. Sadly, during matching UI implementation I realized what I'm not gonna finish it as even it was comand line UI I was nearly building a comand line UI framework. So I decided to leave nearly two days work and start again, just this time simple UI without any ability change any settings... it was last subbmission day..

In UI you can:
- **Check input vector setup information**- any change in code (Runner) would effet it there. Here is no way to change input vector structure from UI as it was way to complex task. My input vector build is way flexible for simple UI.
-  **Train NN** - displays network topology and trains network with the `wili` file. When training is done it saves it automaticaly. Name of the file is as `new Date().toString()`.Note: as in the input vector case network can be changed only form code (Runner) due same reasons.
-  **Load preprained network** - load from file previously trained network. Note: File should have no spaces!
-  **Predict** - allows to choose a file and NN trys to predict language of that file content.

###### Training stats
During training stats will be shown as:
```bash
Training stats:
    Epoch: 2,                       # epoch
    Epoch time: 20.10 sec (+0.12),  # epoch time and its change from previous one
    Train error: 0.4788% (-0.13),   # error (*100) and its change 
    Estimated time left: 140 sec    # aproximate time left
```
Run time is set to `160 sec`, but if need acurs can be changed from inside the `Runner` class.
Note: 
- Those stats are `async` and implemented using Java reactive features, without any external library. It would grately benefit more complex UI as ot ist non-blocking nature, but it is a bit gimmick in current situation.
- After training is done, test are run against the training data. Example bellow:
```bash
INFO: Training complete: error: 0.0017292323897862873, epochs: 9, total time: 180.71989934
-------------------
TestStats: {            # test stats
    total: 11750,       # total entries tested
    right: 8606,        # of them guessed right
    ratio: 73.2%        # corect ratio
    sensitivity: 0.80   # sensitivity
}


---------------------------
   Train - Neural network       # Training menu bellow
---------------------------
```

### Extras
I will list extras I did, but not sure how relevant they are.
- Java Streams and FP just to make code easier to understand and maintain.
- Relatively clean code (add UI in a hurry messed some of it).
- Java-14 and its preview features made code much easier to understand.
- Ability to create input vector of any size and shape (As I cover earlier).
- Data preparation (As I cover earlier).
- No `csv` file step.
- Fast training time (max:  160 sec + epoch time (~20 sec)).
- Reactive Java: training stats are reactive and in use, reactive data preparation - not in use as encog doesn't support reactive Java yet (I had hoped to integrate it myself but time was an issue).
- Logger.
- I wish I have not done it, but I have half done Command Line UI framework.

As I am a believer what clean code pays of and is why here are nearly all extras is related to it.

### Conclusion
My network doesn't predict well (~70%), but in another hand it doesn't use much time to train either (~3min on 6 year old 4th gen i7). I could maybe increase precision by ~ 10% but with the cost of training time, and I didn't wanted to do it. I am probably wouldn't never finish this project if not tips and ideas I got from the Moodle forum. Those tips helped me keep going at the times I was stuck. Special thanks to Ronan and to lecturer Dr.John Healey.

Biggest misstate I made was related to UI. I wanted to create a flexible UI to match my application capabilities, but it was a wrong and time-consuming mistake.



